; Drupal.org release file.
core = 7.x
api = 2

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc5"

projects[ajax_comments][subdir] = "contrib"
projects[ajax_comments][version] = "1.2"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.9"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.6"

projects[features][subdir] = "contrib"
projects[features][version] = "2.7"

projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = "1.0-rc3"

projects[nodejs][subdir] = "contrib"
projects[nodejs][version] = "1.9"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.5"

projects[privatemsg][subdir] = "contrib"
projects[privatemsg][version] = "1.4"

projects[privatemsg_nodejs][subdir] = "contrib"
projects[privatemsg_nodejs][version] = "1.0"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.9"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[url_formatter][subdir] = "contrib"
projects[url_formatter][version] = "1.0-alpha2"

projects[url_formatter_privatemsg][subdir] = "contrib"
projects[url_formatter_privatemsg][version] = "1.x-dev"

projects[views][subdir] = "contrib"
projects[views][version] = "3.11"

projects[views_nodejs][subdir] = "contrib"
projects[views_nodejs][version] = "1.0"

projects[rules_nodejs_action][subdir] = "contrib"
projects[rules_nodejs_action][version] = "1.x-dev"


