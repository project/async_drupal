<?php
/**
 * @file
 * system_variables.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function system_variables_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ajax_comments_node_types';
  $strongarm->value = array(
    'article' => 'article',
    'page' => 'page',
    'news' => 'news',
  );
  $export['ajax_comments_node_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ajax_comments_notify';
  $strongarm->value = 0;
  $export['ajax_comments_notify'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rules_empty_sets';
  $strongarm->value = array(
    'privatemsg_insert' => 0,
    'node_update' => 1,
    'node_presave' => 2,
    'node_view' => 3,
    'node_delete' => 4,
    'init' => 5,
    'cron' => 6,
    'watchdog' => 7,
    'user_insert' => 8,
    'user_update' => 9,
    'user_presave' => 10,
    'user_view' => 11,
    'user_delete' => 12,
    'user_logout' => 13,
    'comment_update' => 14,
    'comment_presave' => 15,
    'comment_view' => 16,
    'comment_delete' => 17,
    'taxonomy_term_insert' => 18,
    'taxonomy_term_update' => 19,
    'taxonomy_term_presave' => 20,
    'taxonomy_term_delete' => 21,
    'taxonomy_vocabulary_insert' => 22,
    'taxonomy_vocabulary_update' => 23,
    'taxonomy_vocabulary_presave' => 24,
    'taxonomy_vocabulary_delete' => 25,
  );
  $export['rules_empty_sets'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'home';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
