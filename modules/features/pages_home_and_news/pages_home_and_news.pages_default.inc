<?php
/**
 * @file
 * pages_home_and_news.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function pages_home_and_news_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home';
  $page->task = 'page';
  $page->admin_title = 'home';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Welcome to ASYNC Drupal!';
  $display->uuid = '9e186a97-e2bb-4807-a1cd-0a6ca7e84865';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a69e0f89-aa52-45a8-9579-d59415aae342';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<strong>ASYNC Drupal</strong> is a distributive which is based on a marvellous platform Node. JS for its great interactivity and performance. This distributive includes main modules, like Views, Rules, Features and many modules for integration with node.js, which provide interactivity with Drupal.
<strong>ASYNC Drupal</strong> provides wonderful possibilities and amazing interactivity for communication.
More info you can find on our <a href="http://www.youtube.com/channel/UCO_HgEycIVml9uB1EV4xErw">YouTube channel</a> watching introducing videos!
Create super interactive sites without knowing the code with little effort! Stay with <strong>ASYNC Drupal</strong>!',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a69e0f89-aa52-45a8-9579-d59415aae342';
    $display->content['new-a69e0f89-aa52-45a8-9579-d59415aae342'] = $pane;
    $display->panels['center'][0] = 'new-a69e0f89-aa52-45a8-9579-d59415aae342';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-a69e0f89-aa52-45a8-9579-d59415aae342';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'news';
  $page->task = 'page';
  $page->admin_title = 'news';
  $page->admin_description = '';
  $page->path = 'news';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_news_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'news';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '77.61018509050007',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'left',
        'width' => '22.389814909499947',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '5b802edb-fef9-489d-ab1d-a564a64e1f3c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-90f16015-3a7e-47be-a835-1f436bc739db';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'views-news-block_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '90f16015-3a7e-47be-a835-1f436bc739db';
    $display->content['new-90f16015-3a7e-47be-a835-1f436bc739db'] = $pane;
    $display->panels['center'][0] = 'new-90f16015-3a7e-47be-a835-1f436bc739db';
    $pane = new stdClass();
    $pane->pid = 'new-88de1c9a-4bf0-426b-81fb-4a25dae19997';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'views-comments-block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '88de1c9a-4bf0-426b-81fb-4a25dae19997';
    $display->content['new-88de1c9a-4bf0-426b-81fb-4a25dae19997'] = $pane;
    $display->panels['left'][0] = 'new-88de1c9a-4bf0-426b-81fb-4a25dae19997';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-90f16015-3a7e-47be-a835-1f436bc739db';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['news'] = $page;

  return $pages;

}
