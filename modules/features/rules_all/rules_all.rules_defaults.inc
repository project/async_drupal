<?php
/**
 * @file
 * rules_all.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rules_all_default_rules_configuration() {
  $items = array();
  $items['rules_add_comment_on_node_news'] = entity_import('rules_config', '{ "rules_add_comment_on_node_news" : {
      "LABEL" : "Add comment on node News",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [
        "rules",
        "rules_nodejs_action",
        "nodejs_marker",
        "views_nodejs",
        "comment"
      ],
      "ON" : { "comment_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "comment:node" ],
            "type" : { "value" : { "news" : "news" } }
          }
        }
      ],
      "DO" : [
        { "rules_nodejs" : {
            "subject" : "Added new comment",
            "body" : "[comment:title]\\r\\n\\r\\n\\u003Ca href=\\u0022[comment:url]\\u0022\\u003EView comment\\u003C\\/a\\u003E",
            "picture" : "[comment:author]",
            "roles" : { "value" : { "2" : "2" } }
          }
        },
        { "nodejs_markap" : { "id" : "news", "text" : "new" } },
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:8:\\u0022comments\\u0022;s:10:\\u0022display_id\\u0022;s:5:\\u0022block\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:8:\\u0022comments\\u0022;s:10:\\u0022display_id\\u0022;s:5:\\u0022block\\u0022;}" } } } }
      ]
    }
  }');
  $items['rules_node_add'] = entity_import('rules_config', '{ "rules_node_add" : {
      "LABEL" : "Add node News",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "nodejs_marker", "views_nodejs", "rules_nodejs_action" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "news" : "news" } } } }
      ],
      "DO" : [
        { "nodejs_markap" : { "id" : "news", "text" : "new" } },
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:4:\\u0022news\\u0022;s:10:\\u0022display_id\\u0022;s:7:\\u0022block_1\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:4:\\u0022news\\u0022;s:10:\\u0022display_id\\u0022;s:7:\\u0022block_1\\u0022;}" } } } },
        { "rules_nodejs" : {
            "subject" : "New content added - [node:title]",
            "body" : "\\u003Ca href=\\u0022[node:url]\\u0022\\u003Eview node\\u003C\\/a\\u003E",
            "picture" : "[site:current-user]",
            "roles" : { "value" : { "2" : "2" } }
          }
        }
      ]
    }
  }');
  $items['rules_user_has_logged_in'] = entity_import('rules_config', '{ "rules_user_has_logged_in" : {
      "LABEL" : "User has logged in",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_nodejs_action" ],
      "ON" : { "user_login" : [] },
      "DO" : [
        { "rules_nodejs" : {
            "subject" : "[account:name]",
            "body" : "has logged in",
            "picture" : "[account:uid]",
            "roles" : { "value" : { "2" : "2" } }
          }
        }
      ]
    }
  }');
  return $items;
}
