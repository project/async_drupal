<?php
/**
 * @file
 * comments_in_news.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function comments_in_news_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'comment-comment_node_news-comment_body'
  $field_instances['comment-comment_node_news-comment_body'] = array(
    'bundle' => 'comment_node_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'url_formatter',
        'settings' => array(
          'UrlFormatterImage' => array(
            'enable' => 1,
            'height' => '100%',
            'width' => '250px',
          ),
          'UrlFormatterYoutube' => array(
            'enable' => 1,
            'height' => '100%',
            'width' => '250px',
          ),
        ),
        'type' => 'url_formatter',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'comment',
    'field_name' => 'comment_body',
    'label' => 'Comment',
    'required' => TRUE,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Comment');

  return $field_instances;
}
