<?php

/**
 * @file
 * Rules integration for the Rules nodejs action module.
 */

/**
 * Implements hook_rules_action_info().
 */
function nodejs_marker_rules_action_info() {
  $items = array();
  $items['nodejs_markap'] = array(
    'label' => t('Add marker'),
    'group' => t('node.js'),
    'parameter' => array(
      'id' => array(
        'type' => 'text',
        'label' => t('The id of html element.'),
        'description' => t('The id of html element to which need add marker.'),
        'default value' => FALSE,
      ),
      'text' => array(
        'type' => 'text',
        'label' => t('The text of marker.'),
        'description' => t('The text of marker which will be added.'),
        'default value' => 'new',
      ),
    ),
    'base' => 'nodejs_marker_rules_process',
  );

  return $items;
}

/**
 * Action: Process and send.
 */
function nodejs_marker_rules_process($id, $text) {
  $marker = theme('nodejs_marker', array('settings' => array('id' => $id, 'text' => t($text))));
  $commands[] = ajax_command_remove('#' . $id . ' .marker', $marker);
  $commands[] = ajax_command_append('#' . $id, $marker);
  $message = (object) array(
    'channel' => 'nodejs_marker',
    'commands' => $commands,
    'callback' => 'nodejsNodeAjax',
  );

  nodejs_send_content_channel_message($message);
}
