api = 2
core = 7.x
projects[drupal][version] = 7.41

; Patches for Core
projects[drupal][patch][] = "https://drupal.org/files/issues/2223595-async_drupal-small-things.patch"
